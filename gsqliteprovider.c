/*
   Copyright (c) 2023 Dotsenko A. N.

   This file is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 2.1 of the
   License, or (at your option) any later version.

   This file is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <sqlite3.h>

#include <stdbool.h>

#include "gsqlite/gsqliteprovider.h"

struct GtkSqliteProviderPrivate {
    sqlite3 *db;
};

static void gtk_sqlite_provider_class_init(GtkSqliteProviderClass *self_class);
static void gtk_sqlite_provider_init(GtkSqliteProvider *self);

G_DEFINE_TYPE_WITH_PRIVATE(GtkSqliteProvider,
                           gtk_sqlite_provider,
                           GTK_TYPE_DB_PROVIDER);

G_DEFINE_QUARK("gtk-sqlite-provider-error-quark", gtk_sqlite_provider_error);

static void object_finalize(GObject *self_object)
{
    GDbProvider *self_dbprovider = G_DB_PROVIDER(self_object);
    GError *error = NULL;
    if (!g_db_provider_close_db(self_dbprovider, &error)) {
        g_critical("%s", error->message);
        g_error_free(error);
    }
    G_OBJECT_CLASS(gtk_sqlite_provider_parent_class)->finalize(self_object);
}

static gboolean db_provider_open_db(GDbProvider *self_dbprovider,
                                    const char *db_name,
                                    GError **error)
{
    GtkSqliteProvider *self = GTK_SQLITE_PROVIDER(self_dbprovider);
    int rc = sqlite3_open(db_name, &self->priv->db);
    if (rc != SQLITE_OK) {
        g_set_error(error,
                    GTK_SQLITE_PROVIDER_ERROR,
                    GTK_SQLITE_OPEN_DB,
                    "%s",
                    sqlite3_errmsg(self->priv->db));
        return false;
    }
    return true;
}

static gboolean
db_provider_close_db(GDbProvider *self_dbprovider, GError **error)
{
    GtkSqliteProvider *self = GTK_SQLITE_PROVIDER(self_dbprovider);
    int rc = sqlite3_close(self->priv->db);
    if (rc != SQLITE_OK) {
        g_set_error(error,
                    GTK_SQLITE_PROVIDER_ERROR,
                    GTK_SQLITE_CLOSE_DB,
                    "%s",
                    sqlite3_errmsg(self->priv->db));
        return false;
    }
    return true;
}

static gboolean db_provider_exec_sql(GDbProvider *self_dbprovider,
                                     const char *sql,
                                     GError **error)
{
    GtkSqliteProvider *self = GTK_SQLITE_PROVIDER(self_dbprovider);

    char *err_text = NULL;
    int rc = sqlite3_exec(self->priv->db, sql, NULL, NULL, &err_text);
    if (rc != SQLITE_OK) {
        g_set_error(error,
                    GTK_SQLITE_PROVIDER_ERROR,
                    GTK_SQLITE_SQL,
                    "%s",
                    err_text);
        sqlite3_free(err_text);
        return false;
    }

    return true;
}

static gboolean
db_provider_append_list_store_by_sql(GDbProvider *self_dbprovider,
                                     const char *sql,
                                     GListStore *store,
                                     GError **error)
{
    GtkSqliteProvider *self = GTK_SQLITE_PROVIDER(self_dbprovider);
    sqlite3_stmt *statements;
    int rc = sqlite3_prepare_v2(self->priv->db, sql, -1, &statements, NULL);
    if (rc != SQLITE_OK) {
        g_set_error(error,
                    GTK_SQLITE_PROVIDER_ERROR,
                    GTK_SQLITE_SQL,
                    "%s",
                    sqlite3_errmsg(self->priv->db));
        return false;
    }

    rc = sqlite3_step(statements);
    while (rc == SQLITE_ROW) {
        GDbRow *row = g_db_row_new();

        for (int i = 0; i < sqlite3_data_count(statements); ++i) {
            const char *name = sqlite3_column_name(statements, i);
            guint col_id =
                    g_db_provider_get_id_by_col_name(self_dbprovider, name);
            switch (sqlite3_column_type(statements, i)) {
            case SQLITE_INTEGER:
                g_db_row_set_int_column(
                        row, col_id, sqlite3_column_int(statements, i));
                break;
            case SQLITE_FLOAT:
                g_db_row_set_double_column(
                        row, col_id, sqlite3_column_double(statements, i));
                break;
            case SQLITE_TEXT:
                g_db_row_set_string_column(
                        row,
                        col_id,
                        (const char *) sqlite3_column_text(statements, i));
                break;
            case SQLITE_BLOB:
                // TODO: Not realized, GValue doesn't support blobs'
                g_assert_not_reached();
                break;
            case SQLITE_NULL:
                g_db_row_set_value(row, col_id, NULL);
                break;
            }
        }
        g_list_store_append(store, row);
        g_object_unref(row);

        rc = sqlite3_step(statements);
    }

    if (rc != SQLITE_DONE) {
        g_set_error(error,
                    GTK_SQLITE_PROVIDER_ERROR,
                    GTK_SQLITE_SQL,
                    "%s",
                    sqlite3_errmsg(self->priv->db));
        goto undo_statements;
    }

    sqlite3_finalize(statements);
    return true;

undo_statements:
    sqlite3_finalize(statements);
    return true;
}

static void gtk_sqlite_provider_class_init(GtkSqliteProviderClass *self_class)
{
    GObjectClass *self_object_class = G_OBJECT_CLASS(self_class);
    self_object_class->finalize = object_finalize;

    GDbProviderClass *db_provider_class = G_DB_PROVIDER_CLASS(self_class);
    db_provider_class->open_db = db_provider_open_db;
    db_provider_class->close_db = db_provider_close_db;
    db_provider_class->exec_sql = db_provider_exec_sql;
    db_provider_class->append_list_store_by_sql =
            db_provider_append_list_store_by_sql;
}

static void gtk_sqlite_provider_init(GtkSqliteProvider *self)
{
    self->priv = gtk_sqlite_provider_get_instance_private(self);
}

GDbProvider *gtk_sqlite_provider_new()
{
    return g_object_new(GTK_TYPE_SQLITE_PROVIDER, NULL);
}
