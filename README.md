# gtksqlite

## Description
The project provides interface to work with SQLite database in GLib applications.

GtkSqliteProvider provides interface to open and query SQLite databases.

## Preparing
1. On Debian-based systems install dependency packages: git, build-essential, cmake, libsqlite3-dev and libgtk-4-dev:
```shell
sudo apt install -y git build-essential cmake libsqlite3-dev libgtk-4-dev
```

## Compiling
1. Clone sources of the project with submodules into a source directory and build the project in the subling build directory:
```shell
mkdir -p ~/gtksqlite/build
cd ~/gtksqlite
git clone --recurse-submodules git@gitlab.com:andrdots/gtksqlite.git ~/gtksqlite/source
cd ~/gtksqlite/build && cmake -DCMAKE_INSTALL_PREFIX=/usr ../source/ && make
```
1. To build deb packages use command:
`cpack -G DEB`

## Using
To use the library add this project as a submodule into your own project and add it into CMakeLists.txt with add_subdirectory(). Then link against the library **gtksqlite**.

## Authors and acknowledgment
* Andrey N. Dotsenko

## License
The source code is licensed under the LGPL 2.1 license.
