/*
   Copyright (c) 2023 Dotsenko A. N.

   This file is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 2.1 of the
   License, or (at your option) any later version.

   This file is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GTK_SQLITE_PROVIDER_
#define GTK_SQLITE_PROVIDER_

#include <glib-object.h>

#include <gdbprovider/gdbprovider.h>

G_BEGIN_DECLS

#define GTK_TYPE_SQLITE_PROVIDER (gtk_sqlite_provider_get_type())
#define GTK_SQLITE_PROVIDER(obj)                                               \
    (G_TYPE_CHECK_INSTANCE_CAST(                                               \
            (obj), GTK_TYPE_SQLITE_PROVIDER, GtkSqliteProvider))
#define GTK_SQLITE_PROVIDER_CLASS(klass)                                       \
    (G_TYPE_CHECK_CLASS_CAST(                                                  \
            (klass), GTK_TYPE_SQLITE_PROVIDER, GtkTSqliteProviderClass))
#define GTK_IS_SQLITE_PROVIDER(obj)                                            \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GTK_TYPE_SQLITE_PROVIDER))
#define GTK_IS_SQLITE_PROVIDER_CLASS(klass)                                    \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GTK_TYPE_SQLITE_PROVIDER))
#define GTK_SQLITE_PROVIDER_GET_CLASS(obj)                                     \
    (G_TYPE_INSTANCE_GET_CLASS(                                                \
            (obj), GTK_TYPE_SQLITE_PROVIDER, GtkSqliteProviderClass))

typedef struct GtkSqliteProvider GtkSqliteProvider;
typedef struct GtkSqliteProviderClass GtkSqliteProviderClass;
typedef struct GtkSqliteProviderPrivate GtkSqliteProviderPrivate;

struct GtkSqliteProviderClass {
    GDbProviderClass parent_class;
};

struct GtkSqliteProvider {
    GDbProvider parent;

    GtkSqliteProviderPrivate *priv;
};

typedef enum {
    GTK_SQLITE_OPEN_DB,
    GTK_SQLITE_CLOSE_DB,
    GTK_SQLITE_SQL,
    GTK_SQLITE_SQL_FINAL,
} GtkSqliteProviderError;
#define GTK_SQLITE_PROVIDER_ERROR (gtk_sqlite_provider_error_quark())

GType gtk_sqlite_provider_get_type();

GDbProvider *gtk_sqlite_provider_new();

G_END_DECLS

#endif /* GTK_SQLITE_PROVIDER_ */
